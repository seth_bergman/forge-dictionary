# Dictionary Forge App

[![Atlassian license](https://img.shields.io/badge/license-Apache%202.0-blue.svg?style=flat-square)](LICENSE) [![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](CONTRIBUTING.md)

This project contains a Forge app written in Typescript. With this app, users can look up the definitions of words on a Confluence page.

![context menu screenshot](context-menu.png)

The app appears in the context menu, which shows when users highlight text on a page. Selecting *Define word* from the 
menu will open a inline dialog with the word's definition. 
The app uses the [confluence:contextMenu](https://developer.atlassian.com/platform/forge/manifest-reference/#confluence-context-menu) module. 

## Requirements

See [Set up Forge](https://developer.atlassian.com/platform/forge/set-up-forge/) for instructions to get set up.

## Quick start

Once you have logged into the CLI (`forge login`), follow the steps below to install the app onto your site:

1. Clone this repository
2. Run `forge register` to register a new copy of this app to your developer account
3. Run `npm install` to install your dependencies
4. Run `forge deploy` to deploy the app into the default environment
5. Run `forge install` and follow the prompts to install the app

## Documentation

See [developer.atlassian.com/platform/forge/](https://developer.atlassian.com/platform/forge) for more documentation and tutorials
explaining Forge.

## Contributions

Contributions to Dictionary are welcome! Please see [CONTRIBUTING.md](CONTRIBUTING.md) for details. 

## License

Copyright (c) 2020 Atlassian and others.
Apache 2.0 licensed, see [LICENSE](LICENSE) file.

[![With ❤️ from Atlassian](https://raw.githubusercontent.com/atlassian-internal/oss-assets/master/banner-cheers-light.png)](https://www.atlassian.com)

